using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePiece : MonoBehaviour
{
    private GameBoard board;
    // Start is called before the first frame update
    void Start()
    {
        board = Camera.main.GetComponent<GameBoard>();
        Invoke("Fall", GameManager.DROP_SECONDS);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
        {
            Fall();
        }

        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
        {
            Rotate(-1);
        }

        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (!CheckLeft())
            {
                transform.position += Vector3.left;
            }
        }
        else if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (!CheckRight())
            {
                transform.position += Vector3.right;
            }
        }
    }

    private void Fall()
    {
        if(CheckStop())
        {
            //done, lock it in
            board.ConvertPiece(this);
            return;
        }

        //otherwise, move it down one
        transform.position += Vector3.down;
        Invoke("Fall", GameManager.DROP_SECONDS);
    }

    private bool CheckLeft()
    {
        //return true if there's a collision (wall or piece)
        //loop through all indiv squares
        foreach (Transform child in transform)
        {
            Vector3 pos = child.position;
            if (pos.x <= 0 ||
                board.CheckSquare(
                    Mathf.RoundToInt(pos.x - 1),
                    Mathf.RoundToInt(pos.y)
                ))
            {
                return true;
            } 
        }

        return false;
    }

    private bool CheckRight()
    {
        //return true if there's a collision (wall or piece)
        //loop through all indiv squares
        foreach (Transform child in transform)
        {
            Vector3 pos = child.position;
            if (pos.x >= GameBoard.BOARD_WIDTH - 1 ||
                board.CheckSquare(
                    Mathf.RoundToInt(pos.x + 1),
                    Mathf.RoundToInt(pos.y)
                ))
            {
                return true;
            }
        }

        return false;
    }

    private bool CheckStop()
    {
        foreach (Transform child in transform)
        {
            Vector3 pos = child.position;
            if (pos.y <= 0 ||
                board.CheckSquare(
                    Mathf.RoundToInt(pos.x),
                    Mathf.RoundToInt(pos.y) - 1)
                )
            {
                return true;
            }
        }

        return false;
    }

    private bool IsPositionInvalid(int x, int y)
    {
        if(x < 0 || x >= GameBoard.BOARD_WIDTH)
        {
            return true;
        }

        if (y < 0 || y >= GameBoard.BOARD_HEIGHT)
        {
            return true;
        }

        return false;
    } 

    private void Rotate(int dir)
    {
        //rotate to check
        transform.Rotate(dir * Vector3.back * 90);

        //check if position is valid &
        //if there's a collision

        foreach (Transform child in transform)
        {
            Vector3 pos = child.position;
            if (IsPositionInvalid(Mathf.RoundToInt(pos.x), Mathf.RoundToInt(pos.y)) ||
                board.CheckSquare(Mathf.RoundToInt(pos.x),Mathf.RoundToInt(pos.y)))
            {
                transform.Rotate(dir * Vector3.back * -90);
                return;
            }
        }

    }
}
