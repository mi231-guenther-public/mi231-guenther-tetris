using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public List<GamePiece> pieces;

    public static float DROP_SECONDS = 0.4f;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<GameBoard>().Init();
        SpawnPiece();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SpawnPiece()
    {
        //choose a piece
        int r = Random.Range(0, pieces.Count);

        //calculate the position
        Vector3 pos = new Vector3(
            (int)(GameBoard.BOARD_WIDTH / 2) - 1,
            GameBoard.BOARD_HEIGHT - 2,
            0
        );

        //instantiate it
        Instantiate(pieces[r], pos, Quaternion.identity);
    }
}
